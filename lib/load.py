import csv
import pandas
from pathlib import Path

home = str(Path.home())
datadir = (f'{home}/dev/projects/sem_sim/data')
fname = 'COWA Clusters - a_002.xlsx'

def load_csv():
    print(f'Loading file: \n   {datadir}/{fname}')

    df = pandas.read_excel(f'{datadir}/{fname}')

    return df
