import gensim
from gensim.models import Word2Vec
import numpy as np
import pandas as pd

from lib.load import load_csv


#model = gensim.models.KeyedVectors.load_word2vec_format('~/bin/word2vec-master/data/GoogleNews-vectors-negative300.bin', binary=True)

output_name = 'test_3'
output_path = (f'results/{output_name}')

df = load_csv()
df.iloc[:, 1] = np.nan


x = [0, 2, 4, 6, 8, 10]
for j in x:
    for row in range(len(df)):
        if row > 0 and isinstance(df.iloc[row, j], str):
            words = [f'{df.iloc[row-1,j]}', f'{df.iloc[row,j]}']
            # dist = model.similarity(words[0], words[1])
            df.iloc[row, j+1] \
                = (f'{words[0]} + {words[1]}')
            # df.iloc[row,j+1] = dist
    #print(f'{df.iloc[:, j:j+2]}')

print(f'Saving file: \n   {output_path}')
df.to_csv(output_path, encoding='utf-8', index=False)
